const WS_URL = "ws://localhost:8080/battle"

const socket = new WebSocket(WS_URL)

export const init = (store) => {
    socket.onmessage = (evt) => {
        console.log(evt);
        const json = JSON.parse(evt.data);
        store.dispatch({
            type: json.type,
            payload: json.payload
        })
    }
}

export const emit = (type, payload) => socket.send(JSON.stringify({type, payload}))