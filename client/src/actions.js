import axios from 'axios'

import * as ActionTypes from './action_types'
import GameState from "./game_states";

const API_URL = "http://localhost:8080"

axios.defaults.baseURL = API_URL

export function initGame() {
    return {
        type: ActionTypes.INIT_GAME
    }
}

export function leaveGame() {
    return {
        type: ActionTypes.LEAVE_GAME
    };
}

export function startGame() {
    return (dispatch, getState, {emit}) => {
        const state = getState();
        axios.post("/start", state.ships).then(resp => {
            dispatch({type: ActionTypes.GAME_STARTED, payload: {id: resp.data.id}});
            emit("READY", {id: resp.data.id});
        })
    }

}

export function fire(x, y) {
    return (dispatch, getState) => {
        dispatch({type: ActionTypes.FIRE});
        axios.post("/fire", {playerId: getState().id, x: x, y: y}).then(resp => {
        })
    }
}

export function randomiseMap() {
    // const index = Math.floor(Math.random() * shipTemplates.length)
    return {
        type: ActionTypes.RANDOMISE_MAP
    }
}

export function moveShip(ship, x, y) {
    return (dispatch, getState) => {
        const state = getState()
        const oldX = ship.x
        const oldY = ship.y

        if(state.gameState !== GameState.STOPPED) return

        ship.x = x
        ship.y = y

        if (isValidMove(state.ships, ship)) {
            dispatch({
                type: ActionTypes.MOVE_SHIP,
                payload: {ships: state.ships}
            })
        } else {
            ship.x = oldX
            ship.y = oldY
        }
    }
}

export function rotateShip(ship) {
    return (dispatch, getState) => {
        const state = getState()
        const oldDir = ship.direction

        if(state.gameState !== GameState.STOPPED) return

        ship.direction = (oldDir === 'horizontal' ? 'vertical' : 'horizontal')

        if (isValidMove(state.ships, ship)) {
            ship.direction = oldDir
            dispatch({
                type: ActionTypes.ROTATE_SHIP,
                payload: {ship: ship}
            })
        } else {
            ship.direction = oldDir
        }
    }
}

function isValidMove(ships, ship) {
    const sb = getBoundary(ship)

    if (isOversize(sb.left, sb.right) || isOversize(sb.top, sb.bottom)) {
        return false;
    }

    for (let s of ships) {
        if (s.id !== ship.id) {
            const asb = getBoundary(s)
            if (isOverlap(sb.top, sb.bottom, asb.top, asb.bottom) && isOverlap(sb.left, sb.right, asb.left, asb.right)) {
                return false;
            }
        }
    }
    return true;
}

function getBoundary(ship) {
    const top = ship.y;
    const left = ship.x;
    let right, bottom;

    if (ship.direction === 'horizontal') {
        right = left + ship.size - 1;
        bottom = top;
    } else {
        right = left;
        bottom = top + ship.size - 1;
    }

    return {left, top, right, bottom}
}

function isOverlap(start, end, otherStart, otherEnd) {
    if (start >= otherStart) {
        return otherEnd >= start;
    } else {
        return end >= otherStart;
    }
}

function isOversize(start, end) {
    return start < 0 || end > 9;
}