import React, { Component } from 'react'
import classnames from 'classnames';

export const CELL_SIZE = 41;
export const CellStates = {
    CLEAR: 0,
    HIT: 1,
    MISSED: 2
};


class Cell extends Component {
    constructor() {
        super();

        this._handleFire = this._handleFire.bind(this);
    }

    _handleFire() {
        const { state, onFire, x, y } = this.props;

        if(onFire && state === CellStates.CLEAR) {
            onFire(y, x);
        }
    }

    render() {
        const { x, y, state } = this.props;
        const css = classnames('cell', {
            'hit': state === CellStates.HIT,
            'missed': state === CellStates.MISSED
        });

        const styles = {
            left: y * CELL_SIZE,
            top: x * CELL_SIZE,
        };

        return (
            <div className={css} style={styles} onClick={this._handleFire}>
            </div>
        )
    }
}

export default Cell;