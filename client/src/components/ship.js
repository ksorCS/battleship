import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {DragSource} from 'react-dnd';

import {CELL_SIZE} from "./cell";

const shipSource = {
    canDrag(props, monitor) {
        return props.canDrag
    },
    beginDrag(props) {
        return props;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

class Ship extends Component {
    constructor() {
        super()

        this._handleRotate = this._handleRotate.bind(this)
    }

    _handleRotate(e) {
        e.preventDefault()
        if (this.props.onRotate)
            this.props.onRotate();
    }

    render() {
        const {isDragging, connectDragSource, ship: {x, y, direction, size}} = this.props;
        const styles = {
            left: x * CELL_SIZE,
            top: y * CELL_SIZE,
            width: (direction === 'horizontal' ? size * CELL_SIZE : CELL_SIZE) + 1,
            height: (direction === 'horizontal' ? CELL_SIZE : size * CELL_SIZE) + 1,
            opacity: isDragging ? 0.5 : 1
        };

        return connectDragSource(
            <div className="ship" style={styles} onContextMenu={this._handleRotate}>
            </div>
        )
    }
}

export default DragSource('SHIP', shipSource, collect)(Ship);