import React, { Component } from 'react'
import { DropTarget } from 'react-dnd';
import {findDOMNode} from 'react-dom';

import classnames from 'classnames'
import Cell from './cell'
import Ship from './ship'

import GameState from '../game_states'

const boardTarget = {
    hover(props, monitor, component) {
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
        const clientOffset = monitor.getSourceClientOffset();
        const hoverClientX = clientOffset.x - hoverBoundingRect.left;
        const hoverClientY = clientOffset.y - hoverBoundingRect.top;
        const x = Math.round(hoverClientX / 42)
        const y = Math.round(hoverClientY/42)

        props.moveShip(monitor.getItem().ship, x, y)
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}

class Board extends Component {
    _renderCells() {
        const { board, onFire } = this.props;

        let cells = [];
        for(let i = 0; i < this.props.size; i++) {
            for(let j = 0; j < this.props.size; j++) {
                cells.push(<Cell onFire={onFire} state={board[i][j]} key={`cell_${i}_${j}`} x={i} y={j}/>);
            }
        }

        return cells;
    }

    _renderShips() {
        const {ships, gameState} = this.props

        return (ships || []).map((ship, index) =>
            <Ship key={`ship_${index}`}
                  ship={ship}
                  canDrag={gameState === GameState.STOPPED}
                  onRotate={() => this.props.onRotate(ship)}/>
        )
    }

    render() {
        const {connectDropTarget} = this.props
        const css = classnames('board', {
            active: this.props.active
        });

        return connectDropTarget(
            <div className={css}>
                {this._renderCells()}
                {this._renderShips()}
            </div>
        )
    }
}

export default DropTarget("SHIP", boardTarget, collect)(Board);;