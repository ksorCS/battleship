import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducer';

import Game from './game';
import {init as initWebSocket, emit} from './websocket'

import registerServiceWorker from './registerServiceWorker';

const middleware = [thunkMiddleware.withExtraArgument({emit})]
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware))
);

initWebSocket(store)

ReactDOM.render(
    <Provider store={store}>
        <Game/>
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
