import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import * as Actions from './actions'
import Board from './components/board'
import GameState from './game_states'

import './game.css'

class Game extends Component {
    constructor() {
        super();

        this._handleFire = this._handleFire.bind(this);
        this._handleRotate = this._handleRotate.bind(this);
    }

    componentDidMount() {
        this.props.initGame()
    }

    _handleRotate(ship) {
        const {gameState, rotateShip} = this.props;
        if (gameState === GameState.STOPPED)
            rotateShip(ship);
    }

    _handleFire(x, y) {
        const {yourTurn, fire} = this.props;
        if (yourTurn)
            fire(x, y);
    }

    render() {
        const {moveShip, yourTurn, myBoard, opponentBoard, ships, startGame, gameState, id, randomiseMap, winner} = this.props;

        return (
            <div>
                <div className="header">
                    <h1>Battleship</h1>
                </div>
                <div className="left">
                    <p>(Your field)</p>
                    <Board moveShip={moveShip}
                           size={10}
                           board={myBoard}
                           ships={ships}
                           gameState={gameState}
                           onRotate={this._handleRotate}/>
                </div>
                <div className="right">
                    <p>(Opponent's field)</p>
                    <Board active={gameState === GameState.READY && yourTurn}
                           size={10}
                           board={opponentBoard}
                           onFire={this._handleFire}/>
                </div>
                <div className="footer">
                {
                    gameState === GameState.READY && yourTurn &&
                    <p>
                        Your Turn
                    </p>
                }
                {
                    gameState === GameState.READY && !yourTurn &&
                    <p>
                        Opponent's Turn
                    </p>
                }
                {
                    gameState === GameState.STOPPED && winner && winner === id &&
                    <p>
                        <h1> You won </h1>
                    </p>
                }
                {
                    gameState === GameState.STOPPED && winner && winner !== id &&
                    <p>
                        <h1> You lose </h1>
                    </p>
                }

                {
                    gameState === GameState.WAITING_FOR_OPPONENT &&
                    <p>
                        Waiting for opponent...&nbsp;
                        <img src="/images/ajax-loader.gif" alt="Waiting for opponent"/>
                    </p>
                }
                {
                    gameState === GameState.STOPPED && <button style={{marginRight: 10}} onClick={() => randomiseMap()}>Random map</button>
                }
                {
                    gameState === GameState.STOPPED && <button onClick={() => startGame()}>Start Game</button>
                }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        myBoard: state.myBoard,
        opponentBoard: state.opponentBoard,
        id: state.id,
        ships: state.ships,
        gameState: state.gameState,
        yourTurn: state.id === state.currentPlayer,
        winner : state.winner
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}

export default DragDropContext(HTML5Backend)(connect(mapStateToProps, mapDispatchToProps)(Game));
