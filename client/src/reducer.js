import update from 'react-addons-update'
import * as ActionTypes from './action_types'

import GameState from './game_states'

const shipTemplates = [
    [
        {id: 1, x: 0, y: 0, size: 4, direction: 'horizontal'},
        {id: 2, x: 1, y: 2, size: 3, direction: 'vertical'},
        {id: 3, x: 3, y: 4, size: 3, direction: 'horizontal'},
        {id: 4, x: 2, y: 8, size: 2, direction: 'vertical'},
        {id: 5, x: 5, y: 6, size: 2, direction: 'horizontal'},
        {id: 6, x: 6, y: 1, size: 2, direction: 'horizontal'},
        {id: 7, x: 0, y: 8, size: 1, direction: 'horizontal'},
        {id: 8, x: 7, y: 8, size: 1, direction: 'horizontal'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 1, size: 1, direction: 'horizontal'},
    ],
    [
        {id: 1, x: 0, y: 1, size: 4, direction: 'vertical'},
        {id: 2, x: 2, y: 4, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'horizontal'},
        {id: 4, x: 3, y: 8, size: 2, direction: 'vertical'},
        {id: 5, x: 6, y: 4, size: 2, direction: 'horizontal'},
        {id: 6, x: 9, y: 1, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 8, size: 1, direction: 'horizontal'},
        {id: 8, x: 7, y: 8, size: 1, direction: 'horizontal'},
        {id: 9, x: 8, y: 6, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 9, size: 1, direction: 'horizontal'},
    ],
    [
        {id: 1, x: 0, y: 6, size: 4, direction: 'vertical'},
        {id: 2, x: 2, y: 4, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'vertical'},
        {id: 4, x: 3, y: 1, size: 2, direction: 'vertical'},
        {id: 5, x: 4, y: 5, size: 2, direction: 'horizontal'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'horizontal'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'horizontal'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'horizontal'},
    ],
    [
        {id: 1, x: 0, y: 8, size: 4, direction: 'horizontal'},
        {id: 2, x: 1, y: 5, size: 3, direction: 'horizontal'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'horizontal'},
        {id: 4, x: 3, y: 2, size: 2, direction: 'horizontal'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'vertical'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'horizontal'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'vertical'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'vertical'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'vertical'},
    ],
    [
        {id: 1, x: 0, y: 5, size: 4, direction: 'vertical'},
        {id: 2, x: 2, y: 5, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'vertical'},
        {id: 4, x: 3, y: 2, size: 2, direction: 'vertical'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'horizontal'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'horizontal'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'vertical'},
    ],
    [
        {id: 1, x: 0, y: 5, size: 4, direction: 'horizontal'},
        {id: 2, x: 2, y: 7, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'horizontal'},
        {id: 4, x: 3, y: 2, size: 2, direction: 'vertical'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'horizontal'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'horizontal'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'horizontal'},
    ],
    [
        {id: 1, x: 0, y: 5, size: 4, direction: 'vertical'},
        {id: 2, x: 2, y: 7, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'horizontal'},
        {id: 4, x: 3, y: 2, size: 2, direction: 'horizontal'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'horizontal'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'vertical'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'horizontal'},
    ],
    [
        {id: 1, x: 0, y: 2, size: 4, direction: 'horizontal'},
        {id: 2, x: 2, y: 7, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'horizontal'},
        {id: 4, x: 2, y: 4, size: 2, direction: 'horizontal'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'vertical'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'horizontal'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'horizontal'},
        {id: 10, x: 9, y: 0, size: 1, direction: 'vertical'},
    ],
    [
        {id: 1, x: 0, y: 2, size: 4, direction: 'horizontal'},
        {id: 2, x: 2, y: 7, size: 3, direction: 'vertical'},
        {id: 3, x: 5, y: 0, size: 3, direction: 'vertical'},
        {id: 4, x: 2, y: 4, size: 2, direction: 'horizontal'},
        {id: 5, x: 5, y: 5, size: 2, direction: 'vertical'},
        {id: 6, x: 7, y: 2, size: 2, direction: 'vertical'},
        {id: 7, x: 0, y: 0, size: 1, direction: 'horizontal'},
        {id: 8, x: 6, y: 8, size: 1, direction: 'vertical'},
        {id: 9, x: 8, y: 5, size: 1, direction: 'vertical'},
        {id: 10, x: 8, y: 0, size: 1, direction: 'vertical'},
    ]
]

const initialState = {
    id: null,
    currentPlayer: null,
    winner : null,
    myBoard: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    opponentBoard: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    ships: shipTemplates[0],
    gameState: GameState.STOPPED
};

export default function appReducer(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.MOVE_SHIP:
            return Object.assign({}, state, { ships: action.payload.ships });
        case ActionTypes.ROTATE_SHIP:
            const ship = action.payload.ship
            return update(state, {ships: {[ship.id - 1]: {direction: {$set: ship.direction === 'horizontal' ? 'vertical' : 'horizontal'}}}});
        case ActionTypes.INIT_GAME:
            return Object.assign({}, state, { gameState: GameState.STOPPED });
        case ActionTypes.GAME_STARTED:
            return Object.assign({}, state, {
                id: action.payload.id,
                gameState: GameState.WAITING_FOR_OPPONENT
            });
        case ActionTypes.GAME_READY:
            return Object.assign({}, state, {
                gameState: GameState.READY,
                currentPlayer: action.payload.currentPlayer
            });
        case ActionTypes.FIRE:
            return Object.assign({}, state, {
                gameState: GameState.WAITING
            });
        case ActionTypes.GAME_UPDATED:
            return Object.assign({}, state, {
                currentPlayer: action.payload.currentPlayer,
                gameState: GameState.READY,
                myBoard: action.payload.myBoard,
                opponentBoard: action.payload.opponentBoard
            });
        case ActionTypes.RANDOMISE_MAP:
            const index = Math.floor(Math.random() * shipTemplates.length)
            return Object.assign({}, state, { ships: shipTemplates[index]});
        case ActionTypes.GAME_OVER:
            return Object.assign({}, state, {
                winner: action.payload.winner,
                myBoard : initialState.myBoard,
                opponentBoard : initialState.opponentBoard,
                gameState : GameState.STOPPED
            });
        default:
            return state;
    }
}