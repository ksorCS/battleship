const GameState = {
    STOPPED: 'STOPPED',
    READY: 'READY',
    WAITING: 'WAITING',
    WAITING_FOR_OPPONENT: 'WAITING_FOR_OPPONENT'
};

export default GameState