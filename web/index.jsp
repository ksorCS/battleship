<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Battleship</title>
    <link rel="icon"
          type="image/png"
          href="<c:url value='resources/ship.png'/>">
  </head>
  <body>
  <div id="root"></div>
  </body>
  <script src='http://localhost:3000/static/js/bundle.js'></script>
</html>
