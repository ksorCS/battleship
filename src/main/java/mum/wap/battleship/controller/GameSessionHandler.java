package mum.wap.battleship.controller;

import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;


public final class GameSessionHandler {
    private static GameSessionHandler instance;
    private final Map<String, Session> sessions = new HashMap<>();
    private final Queue<Session> waitingSession = new ArrayDeque<>();

    private GameSessionHandler(){

    }

    public static GameSessionHandler getInstance(){
        if(instance == null){
            instance = new GameSessionHandler();
        }
        return  instance;
    }

    public void addSession(Session session) {
        sessions.put(session.getId(), session);
    }

    public void addToWaitingList(Session session){
        waitingSession.add(session);
    }

    public void removeSession(Session session) {
        sessions.remove(session.getId());
    }

    public  Session getAvailableSession(){
        if(waitingSession.size() ==0) return null;
        Session session = waitingSession.poll();
        if(session.isOpen()){
            return session;
        }else {
            return getAvailableSession();
        }
    }


    public void sendToSession(Session session, String message){
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException ex) {
            sessions.remove(session);
            System.out.println(ex);
        }
    }

    public Session getSessionById(String id){
        return sessions.get(id);
    }
}
