package mum.wap.battleship;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import mum.wap.battleship.model.GameManager;
import mum.wap.battleship.model.Match;
import mum.wap.battleship.model.Player;
import mum.wap.battleship.model.Ship;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@WebServlet(name = "StartGameServlet", urlPatterns = {"/start"})
public class StartGameServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = br.readLine();

        ObjectMapper mapper = new ObjectMapper();
        List<Ship> ships = mapper.readValue(json, new TypeReference<List<Ship>>(){});

        GameManager gameManager = GameManager.getInstance();
        Player player = gameManager.newPlayer();
        player.setShips(ships);

        Match match = gameManager.getAvailableMatch();

        if(match == null) {
            match = gameManager.newMatch();
            match.setPlayer1(player);
        } else {
            match.setPlayer2(player);
        }

        ObjectNode result = mapper.createObjectNode();
        result.put("id", player.getId());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result.toString());
    }
}
