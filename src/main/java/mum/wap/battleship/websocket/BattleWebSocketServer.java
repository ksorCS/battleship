package mum.wap.battleship.websocket;

import mum.wap.battleship.model.GameManager;
import mum.wap.battleship.model.Player;
import mum.wap.battleship.util.JsonObjectConverter;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;

@ServerEndpoint("/battle")
public class BattleWebSocketServer {

    @OnOpen
    public void open(Session socket) {
    }

    @OnClose
    public void close(Session socket) {
        Map<String, Object> properties = socket.getUserProperties();
        Player player = GameManager.getInstance().getPlayer((String)properties.get("playerId"));

        if(player != null) {
            GameManager.getInstance().playerLeft(player);
        }
    }

    @OnError
    public void onError(Throwable error) {
        System.out.println(error);
    }

    @OnMessage
    public void handleMessage(String message, Session socket) {
        Map<String, Object> messageMap = JsonObjectConverter.jsonToObject(message, Map.class).orElse(new HashMap());
        Map<String, Object> payload = (Map)messageMap.get("payload");

        switch((String)messageMap.get("type")){
            case "READY":
                Map<String, Object> properties = socket.getUserProperties();
                Player player = GameManager.getInstance().getPlayer((String)payload.get("id"));

                properties.put("playerId", player.getId());
                player.setSocket(socket);
                player.ready();
                break;
        }
    }
}
