package mum.wap.battleship.model;

import com.fasterxml.jackson.annotation.JsonValue;

public class Ship {
    private int id;
    private int x;
    private int y;
    private int size;
    private Direction direction;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public enum Direction {
        HORIZONTAL("horizontal"),
        VERTICAL("vertical");
        private String value;

        Direction(String value) {
            this.value = value;
        }
        @JsonValue
        public String getValue() {
            return value;
        }
    }
}
