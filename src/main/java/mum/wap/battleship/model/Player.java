package mum.wap.battleship.model;

import com.fasterxml.jackson.annotation.JsonValue;

import javax.websocket.Session;
import java.util.List;

public class Player {
    private String id;
    private List<Ship> ships;
    private Match match;
    private boolean ready;
    private Session socket;
    private int[][] board = new int[10][10];
    private boolean[][] shipPositions = new boolean[10][10];

    public Player(String id) {
        this.id = id;
        this.ready = false;
    }

    public void ready() {
        this.ready = true;
        match.start();
    }

    public boolean getReady() {
        return ready;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
        initShipPosition();
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public Session getSocket() {
        return socket;
    }

    public void setSocket(Session socket) {
        this.socket = socket;
    }

    public boolean shoot(Shot shot){
        int x = shot.getX();
        int y = shot.getY();
        int status = CellStatus.EMPTY.getValue();
        if(shipPositions[y][x]){
            status = CellStatus.HIT.getValue();
        }else{
            status = CellStatus.MISS.getValue();
        }
        board[y][x] = status;
        if(status == CellStatus.HIT.getValue()){
            return true;
        }
        return false;
    }

    public void initShipPosition(){
        for(Ship ship : ships){
            int x = ship.getX();
            int y = ship.getY();
            if(ship.getDirection() == Ship.Direction.VERTICAL){
                for(int i = 0 ; i < ship.getSize() ; i ++){
                    shipPositions[y+i][x]= true;
                }
            }else{
                for(int i = 0 ; i < ship.getSize() ; i ++){
                    shipPositions[y][x+i]= true;
                }
            }
        }
    }

    public boolean isLose() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (shipPositions[i][j]) {
                    if (board[i][j] != CellStatus.HIT.value) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    public int[][] getBoard() {
        return board;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }


    public enum CellStatus{
        HIT(1),
        MISS(2),
        EMPTY(0);

        CellStatus(int value) {
            this.value = value;
        }

        private int value;

        @JsonValue
        public int getValue() {
            return value;
        }
    }
}
