package mum.wap.battleship.model;

import java.util.*;

public class GameManager {
    private static GameManager instance = new GameManager();
    private List<Match> availableMatches;
    private Map<String, Match> matches;
    private Map<String, Player> players;

    private GameManager() {
        this.players = new HashMap<>();
        this.matches = new HashMap<>();
        this.availableMatches = new ArrayList<>();
    }

    public static GameManager getInstance(){
        return instance;
    }

    public Match newMatch() {
        Match match = new Match(UUID.randomUUID().toString());
        matches.put(match.getId(), match);
        availableMatches.add(match);

        return match;
    }

    public Player newPlayer() {
        Player player = new Player(UUID.randomUUID().toString());
        this.players.put(player.getId(), player);
        return player;
    }

    public Match getAvailableMatch() {
        if(availableMatches.size() > 0) {
            return availableMatches.remove(0);
        }

        return null;
    }

    public Player getPlayer(String id) {
        return players.get(id);
    }

    public void playerLeft(Player player) {
        Match match = player.getMatch();

        match.end();

        this.players.remove(player.getId());
        this.matches.remove(match.getId());
    }

    public Optional<Match> getMatchByPlayerId(String id) {
        return this.matches.values().stream()
                .filter(match -> match.isMatchOfPlayer(id))
                .findAny();
    }

}
