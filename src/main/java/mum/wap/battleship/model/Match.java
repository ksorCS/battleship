package mum.wap.battleship.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class Match {
    private String id;

    private Player player1;
    private Player player2;
    private boolean isStarted;
    private String currentPlayerId;

    public Match(String id) {
        this.id = id;
        this.isStarted = false;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player) {
        player.setMatch(this);

        this.currentPlayerId = player.getId();
        this.player1 = player;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player) {
        player.setMatch(this);
        this.player2 = player;
    }

    public String getId() {
        return this.id;
    }


    public boolean isMatchOfPlayer(String id){
        return id.equals(this.player1.getId())
                || id.equals(this.player2.getId());
    }

    private Player getPlayer(String id) {
        if (id.equals(this.player1.getId())) {
            return this.player1;
        } else {
            return this.player2;
        }
    }

    private Player getOpponent(String playerId) {
        if (playerId.equals(this.player1.getId())) {
            return this.player2;
        } else {
            return this.player1;
        }
    }


    public void start() {
        if (this.isStarted)
            return;

        if (player1 != null && player2 != null && player1.getReady() && player2.getReady()) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                ObjectNode result = mapper.createObjectNode();
                result.put("type", "GAME_READY");

                ObjectNode payload = result.putObject("payload");
                payload.put("currentPlayer", player1.getId());

                player1.getSocket().getBasicRemote().sendText(result.toString());
                player2.getSocket().getBasicRemote().sendText(result.toString());
                this.isStarted = true;
            } catch (IOException ex) {
                this.isStarted = false;
            }
        }
    }

    public void shoot(Shot shot){
        Player currentPlayer = getPlayer(shot.getPlayerId());
        Player opponent =  getOpponent(shot.getPlayerId());
        boolean isHit = opponent.shoot(shot);
        boolean isCurrentPlayerWin = opponent.isLose();
        if(isCurrentPlayerWin){
            end();
        }else{
            sendUpdateMessage(currentPlayer, opponent, isHit);
        }
    }

    private void sendUpdateMessage(Player player, Player opponent, boolean isHit) {
        String nextPlayerId = opponent.getId();
        if (isHit) {
            nextPlayerId = player.getId();
        }
        this.currentPlayerId = nextPlayerId;
        int[][] myBoard = player.getBoard();
        int[][] opponentBoard = opponent.getBoard();
        try {
            player.getSocket().getBasicRemote().sendText(createShotPayloadMessage(myBoard, opponentBoard, nextPlayerId));
            opponent.getSocket().getBasicRemote().sendText(createShotPayloadMessage(opponentBoard, myBoard, nextPlayerId));
        } catch (IOException e) {
            System.out.println("Can not send update to players");
            e.printStackTrace();
        }
    }

    private String createShotPayloadMessage(int[][] myBoard, int[][] opponentBoard, String nextPlayerId) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode result = mapper.createObjectNode();
        result.put("type", "GAME_UPDATED");

        JsonNode myBoardNode  = mapper.convertValue(myBoard, JsonNode.class);
        JsonNode oppBoardNode  = mapper.convertValue(opponentBoard, JsonNode.class);

        ObjectNode payload = mapper.createObjectNode();
        payload.set("myBoard", myBoardNode);
        payload.set("opponentBoard", oppBoardNode);
        payload.put("currentPlayer", nextPlayerId);

        result.set("payload", payload);
        return result.toString();
    }

    public void end() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode result = mapper.createObjectNode();
            result.put("type", "GAME_OVER");

            ObjectNode payload = mapper.createObjectNode();
            String winnerID  = player1.getId();
            if(!player1.getSocket().isOpen() || player1.isLose()){
                winnerID = player2.getId();
            }
            payload.put("winner", winnerID);
            result.set("payload", payload);


            if(player1.getSocket().isOpen())
                player1.getSocket().getBasicRemote().sendText(result.toString());

            if(player2.getSocket().isOpen())
                player2.getSocket().getBasicRemote().sendText(result.toString());

            this.isStarted = true;
        } catch (IOException ex) {
            this.isStarted = false;
        }
    }

    public void fire() {

    }
}
