package mum.wap.battleship.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mum.wap.battleship.model.Shot;

import java.util.Optional;

public final  class JsonObjectConverter {
    public static  String toJsonString(Object object){
        ObjectMapper mapper = new ObjectMapper();
        String message = null;
        try {
            message = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return  message;
    }

    public static <T> Optional<T> jsonToObject(String jsonString, Class<T> clazz){
        try {
            ObjectMapper mapper = new ObjectMapper();
            return Optional.ofNullable(mapper.readValue(jsonString, clazz));
        } catch (Exception e) {
            System.out.print(e);
            return Optional.empty();
        }
    }
}
