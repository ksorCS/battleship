package mum.wap.battleship;

import mum.wap.battleship.model.GameManager;
import mum.wap.battleship.model.Shot;
import mum.wap.battleship.util.JsonObjectConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

@WebServlet(name = "FireServlet", urlPatterns = "/fire")
public class FireServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = br.readLine();
        Optional<Shot> shotOptional = JsonObjectConverter.jsonToObject(json, Shot.class);

        GameManager gameManager = GameManager.getInstance();
        if (shotOptional.isPresent()) {
            Shot shot = shotOptional.get();
            String playerId = shot.getPlayerId();
            gameManager.getMatchByPlayerId(playerId)
                .ifPresent(match -> match.shoot(shot));
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("");
    }
}
